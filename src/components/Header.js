import React from 'react';

import '../css/header.css';

const Header = () => {
    return (
        <header className='main-header'>
            <div>Logo</div>
            <nav className='main-nav'>
                <a href='/'>Create Ads</a>
                <a href='/'>About Us</a>
                <a href='/'>Log In</a>
                <a href='/' className='register-btn'>
                    Register
                </a>
            </nav>
        </header>
    );
};

export default Header;
